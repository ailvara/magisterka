package eu.eglasses.robot.example;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.parrot.arsdk.arcontroller.ARDeviceController;
import com.parrot.arsdk.arcontroller.ARFrame;

import java.io.ByteArrayInputStream;

/**
 * Created by Sylwia Mikolajczuk
 * Activity for interaction with the accelerometer
 */
public class AcceActivity extends RobotActivity
{
    private static String TAG = JoystickActivity.class.getSimpleName();

    private RobotHandler robot;

    SensorManager sensorManager;
    Sensor accelerometer;
    SensorObserver observer;

    /**
     * Observer class for registered value changes
     */
    class SensorObserver implements SensorEventListener
    {
        private float x,y,z,lastz;


        /**
         * When sensor values change, interpret the motion
         * @param event data from the sensor
         */
        @Override
        public void onSensorChanged(SensorEvent event)
        {
            switch(event.sensor.getType())
            {
                case Sensor.TYPE_ACCELEROMETER:
                    x = event.values[0];
                    y = event.values[1];
                    z = event.values[2];

                    if(robot!=null) {
                        //make turn
                        //if(Math.abs(y)>2) robot.turn((int) (Math.signum(y)*Math.min(15,5+(int)Math.abs(y))));
                        if(y>2) robot.turn(Math.min(15,3*(int)y));
                        else if (y<-2) robot.turn((-1)*Math.min(15,3*(int)Math.abs(y)));
                        else robot.stopTurn();
                        //make motion
                        if(x>9) robot.move(-20);
                        else if(x<4) robot.move(Math.min(40, 2*Math.abs(6 - (int) x) * 3));
                        else robot.stopMove();
                        //make a jump
                        if (lastz!=0 && Math.abs(z-lastz) > 10) {
                            robot.jumpHigh();
                        }
                    }
                    Log.i("ACC",x+" "+y+" "+z);
                    lastz = z;
                    break;
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {
        }
    }

    /**
     * Init the activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(eu.eglasses.robot.example.R.layout.activity_plain);
        batteryLabel = (TextView) findViewById(R.id.batteryLabel);
        imgView = (ImageView) findViewById(R.id.preview);
        robot = new RobotHandler(deviceController, this);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        observer = new SensorObserver();
    }

    /**
     * Register listeners on start
     */
    @Override
    public void onStart()
    {
        super.onStart();
        if (accelerometer!=null)
            sensorManager.registerListener(observer,accelerometer,
                    SensorManager.SENSOR_DELAY_FASTEST);
    }

    Bitmap bmp;

    /**
     *
     * @param deviceController
     * @param frame
     */
    @Override
    public void onFrameReceived(ARDeviceController deviceController, ARFrame frame)
    {
        if (!frame.isIFrame())
            return;

        byte[] data = frame.getByteData();
        ByteArrayInputStream ins = new ByteArrayInputStream(data);
        bmp = BitmapFactory.decodeStream(ins);

        runOnUiThread (new Thread(new Runnable() {
            public void run() {
                if(bmp!=null){
                    if (imgView != null) {
                        imgView.setImageBitmap(bmp);
                    }
                }
            }
        }));
    }
}



