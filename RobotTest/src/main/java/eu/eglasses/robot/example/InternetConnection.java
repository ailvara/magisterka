package eu.eglasses.robot.example;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.Arrays;
import java.util.Calendar;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class InternetConnection {

    public static InternetConnection instance;
    boolean established;
    boolean bound = false;

    private Context context;
    private String TAG = "ConnectionTest";
    private ConnectivityManager cm;
    private String url = "http://remotesumo.herokuapp.com";
    private Socket socket;
    private RobotHandler robot;

    private long time1,time2;

    InternetConnection(Context c)
    {
        context=c;
        establishConnection(c);
    }

    void giveRobot(RobotHandler robot)
    {
        this.robot = robot;
    }

    private void establishConnection(Context context)
    {
        cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkRequest.Builder req = new NetworkRequest.Builder();
        req.addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET);
        req.addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR);

        cm.requestNetwork(req.build(), new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                try {
                    Log.i(TAG, "Found mobile: " + cm.getNetworkInfo(network).toString());
                    cm.bindProcessToNetwork(network);
                    Log.i(TAG, "Active network after binding " + cm.getActiveNetworkInfo().toString());
                    socket = IO.socket(url);
                    socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                        @Override
                        public void call(Object... args) {
                            Log.i(TAG, "We have connection");
                        }
                    })
                            .on("command", new Emitter.Listener() {
                                @Override
                                public void call(Object... args) {
                                    Log.i(TAG, "Command received: " + args[0]);
                                    if (robot != null) robot.execute(args);
                                }
                            })
                            .on("image", new Emitter.Listener() {
                                @Override
                                public void call(Object... args) {
                                    Log.i("runTest", "Image delivered " + Calendar.getInstance().getTimeInMillis());
                                }
                            })
                            .on("info", new Emitter.Listener(){
                                @Override
                                public void call(Object... args) {
                                    Log.i(TAG,"Info: "+args[0]);
                                }
                            })
                            .on("byte", new Emitter.Listener(){
                                @Override
                                public void call(Object... args) {
                                    long time3 = Calendar.getInstance().getTimeInMillis();
                                    //Log.i(TAG,"Time: "+time1+" "+time2+" "+time3);
                                    Log.i(TAG,"Time diff: "+(time2-time1)+" "+(time3-time2));
                                    time1=0;time2=0;
                                    Log.i(TAG,"Bytes coming: "+args[0]);
                                }
                            })
                            .on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                                @Override
                                public void call(Object... args) {
                                    Log.i(TAG, "Socket disconnected");
                                }
                            });
                    socket.connect();
                    while(!socket.connected()){
                        Thread.sleep(500);
                    }
                    socket.emit("info","before bound");
                    Log.i(TAG, "Bound!");
                    bound = true;
                    socket.emit("info","after bound");
                } catch (URISyntaxException e) {
                    Log.e(TAG, "Problem establishing connection");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void backToWifi()
    {
        Log.i(TAG,"Back to wifi");
        NetworkRequest.Builder r = new NetworkRequest.Builder();
        r.addTransportType(NetworkCapabilities.TRANSPORT_WIFI);
        cm.requestNetwork(r.build(), new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                Log.i(TAG, "Wifi available: " + cm.getNetworkInfo(network));
                cm.bindProcessToNetwork(network);
                Log.i(TAG, "Wifi established: " + cm.getActiveNetworkInfo());
                Log.i(TAG, cm.getActiveNetworkInfo().toString());
                socket.emit("info", "still alive");
                established = true;
            }
        });
    }

    class ExecuteConnection extends AsyncTask<Object, Integer, Integer> {

        @Override
        protected Integer doInBackground(Object... params)
        {
            //if((int)params[0]==7) Log.i(TAG, "Picture!!!");
            if(params.length>=2) {
                String data = params[1].toString();
                //Log.i(TAG, "DATA: "+data);
                socket.emit("image",data);
                Log.i("runTest", "Time: "+Calendar.getInstance().getTimeInMillis()+" Base64 length: " + params.length);
                Log.i("Base64_length",""+data.length());
            }
            Log.i(TAG,"emitting "+socket.connected());
            socket.emit("info", "all ok");
            return 1;
        }
    }

    class SendByte extends AsyncTask<byte[], Integer, Integer>
    {
        @Override
        protected Integer doInBackground(byte[]... data) {
            String send = null;
            //try {
            //send = new String(data[0],"UTF-8");
            //IntBuffer intBuf =
            //        ByteBuffer.wrap(data[0]).order(ByteOrder.BIG_ENDIAN)
            //                .asIntBuffer();
            //int[] array = new int[intBuf.remaining()];
            //intBuf.get(array);
            //JSONArray json = new JSONArray(Arrays.asList(data));
            if(time1==0) time1 = Calendar.getInstance().getTimeInMillis();
            String base64Encoded = Base64.encodeToString(data[0],Base64.DEFAULT);
            socket.emit("byte",base64Encoded);
            if(time2==0) time2 = Calendar.getInstance().getTimeInMillis();
            //} catch (UnsupportedEncodingException e) {
            //    e.printStackTrace();
            //}
            return 1;
        }
    }
}