package eu.eglasses.robot.example;

import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Sylwia Mikolajczuk
 * Activity for interaction with touch gestures
 */
public class JoystickActivity extends RobotActivity
{
    private static String TAG = JoystickActivity.class.getSimpleName();

    private RobotHandler robot;

    GestureDetector gestureDetector;
    private int initx;
    private int inity;

    /**
     * Init the activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(eu.eglasses.robot.example.R.layout.activity_plain);
        imgView = (ImageView) findViewById(R.id.preview);
        batteryLabel = (TextView) findViewById(R.id.batteryLabel);
        robot = new RobotHandler(deviceController, this);
        gestureDetector = new GestureDetector(getApplicationContext(), new GestureListener());
    }

    /**
     * Send the gesture to both intepreters
     * @param event touch event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        processGesture(event);
        gestureDetector.onTouchEvent(event);
        return true;
    }

    /**
     * Listen for common gestures: long press and double tap
     * Fire jump when present
     */
    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public void onLongPress(MotionEvent e) {
            robot.jumpLong();
            Log.i(TAG,"Jump long");
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            robot.jumpHigh();
            Log.i(TAG,"Jump high");
            return true;
        }
    }

    /**
     * Calculate touch gesture distance
     * Remember the starting point
     * Move at x axis distance and turn at y axis distance
     * Terminate at action up
     * @param event touch event
     */
    private void processGesture (MotionEvent event)
    {
        int x = (int)event.getX();
        int y = (int)event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                Log.i("ACTION","down");
                initx=x;
                inity=y;
                //if(Math.abs(initx-x)<50 && Math.abs(inity-y)<50) robot.jumpHigh();
                break;
            case MotionEvent.ACTION_MOVE:
                Log.i("ACTION", "move");
                int distx = initx - x;
                int disty = inity - y;
                robot.move(disty/8);
                robot.turn(-distx/30);
                Log.i("ACTION",distx+" "+disty);
                break;
            case MotionEvent.ACTION_UP:
                robot.stopMove();
                robot.stopTurn();
                Log.i("ACTION","up");
                break;
        }
    }
}



