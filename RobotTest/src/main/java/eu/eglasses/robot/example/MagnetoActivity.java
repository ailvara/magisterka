package eu.eglasses.robot.example;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * The activity for interaction with magnetometer and proximity sensor
 */
public class MagnetoActivity extends RobotActivity
{
    private static String TAG = MagnetoActivity.class.getSimpleName();
    RobotHandler robot;

    SensorManager sensorManager;
    Sensor magneto;
    Sensor prox;
    Sensor light;
    SensorObserver observer;

    /**
     * Init the activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plain);
        imgView = (ImageView) findViewById(R.id.preview);
        batteryLabel = (TextView) findViewById(R.id.batteryLabel);

        robot = new RobotHandler(deviceController, this);
        Log.i(TAG,"onCreate");
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        magneto = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        prox = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        light = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        observer = new SensorObserver();
    }

    /**
     * Clean up before stop
     */
    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i(TAG, "onStart");
        sensorManager.unregisterListener(observer);
        robot.stopTurn();
        robot.stopMove();
    }

    /**
     * Register listeners on start
     */
    @Override
    public void onStart()
    {
        super.onStart();
        Log.i(TAG,"onStart");
        if (magneto!=null)
            sensorManager.registerListener(observer,magneto,SensorManager.SENSOR_DELAY_FASTEST);
        if(prox!=null)
            sensorManager.registerListener(observer,prox,SensorManager.SENSOR_DELAY_FASTEST);
        if(light!=null)
            sensorManager.registerListener(observer,light,SensorManager.SENSOR_DELAY_FASTEST);
        else Log.i("Light", "No light, no light in your bringht blue eyes");
    }

    /**
     * Observer for monitoring sensor changes
     */
    class SensorObserver implements SensorEventListener
    {
        float x,y,z,lastx,lasty,lastz,dx,dy,dz;
        boolean left, right;

        @Override
        public void onSensorChanged(SensorEvent event)
        {
            switch(event.sensor.getType()){
                //Experimenting with light sensor
                case Sensor.TYPE_LIGHT:
                    Log.i("Light",""+event.values[0]);
                    break;
                //Turn left when proximmity is zero
                case Sensor.TYPE_PROXIMITY:
                    float p = event.values[0];
                    Log.i("prox","Proximity change: "+p); //0.0 jak blisko, 5.0 gdy nie
                    if(p<1.0f) {
                        if(robot!=null) robot.turn(-15);
                        Log.i("prox","go");
                    }
                    else {
                        if(robot!=null)robot.stopTurn();
                        Log.i("prox", "stop");
                    }
                    break;
                //Move when magnetic field is large
                case Sensor.TYPE_MAGNETIC_FIELD:
                    if(lastx==0 && lasty==0 && lastz==0) {
                        lastx = event.values[0];
                        lasty = event.values[1];
                        lastz = event.values[2];
                        break;
                    }
                    x = event.values[0];
                    y = event.values[1];
                    z = event.values[2];
                    dx = x-lastx;
                    dy = y-lasty;
                    dz = z-lastz;


                    double change = Math.sqrt(x*x+y*y+z*z);
                    Log.i("Magneto change",""+change);

                    if(change>300) robot.move(35);
                    else robot.stopMove();

                    Log.i(TAG, (int)(x*x)  + " \t" + (int)(y*y) + " \t" + (int)(z*z));
                    lastx = x;
                    lasty = y;
                    lastz = z;
                    break;
            }
        }

        /**
         * On accuracy changed
         * @param sensor
         * @param i
         */
        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {
        }
    }
}
