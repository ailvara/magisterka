package eu.eglasses.robot.example;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;

/**
 * Created By Sylwia Mikolajczuk
 * The activity for choosing mode of interaction
 */
public class ModeActivity extends Activity {

    public static String EXTRA_DEVICE_SERVICE = "extra.device.service";
    public ARDiscoveryDeviceService service;

    /**
     * Init the activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode);
        Intent intent = getIntent();
        service = intent.getParcelableExtra(EXTRA_DEVICE_SERVICE);
    }

    /**
     * Launch ButtonActivity
     * @param button
     */
    public void launchButton(View button)
    {
        Intent intent = new Intent(ModeActivity.this, ButtonActivity.class);
        intent.putExtra(ButtonActivity.EXTRA_DEVICE_SERVICE, service);
        startActivity(intent);
    }

    /**
     * Launch JoystickActivity
     * @param button
     */
    public void launchJoystick(View button)
    {
        Intent intent = new Intent(ModeActivity.this, JoystickActivity.class);
        intent.putExtra(JoystickActivity.EXTRA_DEVICE_SERVICE, service);
        startActivity(intent);
    }

    /**
     * Launch AcceActivity
     * @param button
     */
    public void launchAcce(View button)
    {
        Intent intent = new Intent(ModeActivity.this, AcceActivity.class);
        intent.putExtra(AcceActivity.EXTRA_DEVICE_SERVICE, service);
        startActivity(intent);
    }

    /**
     * Launch RemoteActivity
     * @param button
     */
    public void launchRemote(View button)
    {
        Intent intent = new Intent(ModeActivity.this, RemoteActivity.class);
        intent.putExtra(RemoteActivity.EXTRA_DEVICE_SERVICE, service);
        startActivity(intent);
    }

    /**
     * Launch SpeechActivity
     * @param button
     */
    public void launchSpeech(View button)
    {
        Intent intent = new Intent(ModeActivity.this, SpeechActivity.class);
        intent.putExtra(SpeechActivity.EXTRA_DEVICE_SERVICE, service);
        startActivity(intent);
    }

    /**
     * Launch MagnetoActivity
     * @param button
     */
    public void launchMagneto(View button)
    {
        Intent intent = new Intent(ModeActivity.this, MagnetoActivity.class);
        intent.putExtra(MagnetoActivity.EXTRA_DEVICE_SERVICE, service);
        startActivity(intent);
    }
}
