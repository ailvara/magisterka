package eu.eglasses.robot.example;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.parrot.arsdk.arcontroller.ARDeviceController;
import com.parrot.arsdk.arcontroller.ARFrame;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Calendar;

public class RemoteActivity extends RobotActivity
{
    private static String TAG = JoystickActivity.class.getSimpleName();

    private RobotHandler robot;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(eu.eglasses.robot.example.R.layout.activity_plain);
        batteryLabel = (TextView) findViewById(R.id.batteryLabel);
        imgView = (ImageView) findViewById(R.id.preview);
        robot = new RobotHandler(deviceController, this);
        InternetConnection.instance.giveRobot(robot);
    }

    Bitmap bmp;
    int i = 0;
    int step = 5;

    @Override
    public void onFrameReceived(ARDeviceController deviceController, final ARFrame frame)
    {
        if (!frame.isIFrame())
            return;

        final byte[] data = frame.getByteData();
        ByteArrayInputStream ins = new ByteArrayInputStream(data);
        bmp = BitmapFactory.decodeStream(ins);
        runOnUiThread(new Thread(new Runnable() {
            public void run() {
                if (bmp != null) {

                    //send
                    if (i-- == 0) { //send every 1000th frame or so

                        Log.i("runTest", "Start: " + Calendar.getInstance().getTimeInMillis());
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        Bitmap a = bmp; //?
                        a = getResizedBitmap(a, 400);
                        //a = toGrayscale(a);
                        a.compress(Bitmap.CompressFormat.JPEG, 10, out);
                        robot.sendImage(out.toByteArray());
                        i = step;
                        Log.i("runTest", a.getWidth() + " " + a.getHeight() + " " + out.toByteArray().length + " ");
                        Log.i("Byte_length",""+out.toByteArray().length);
                        if (imgView != null) {
                            imgView.setImageBitmap(a);
                        }
                    }
                    //end send

                    if (imgView != null) {
                        //imgView.setImageBitmap(bmp);
                    }
                }
            }
        }));
    }

    //http://stackoverflow.com/questions/15759195/reduce-size-of-bitmap-to-some-specified-pixel-in-android
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    //http://stackoverflow.com/questions/3373860/convert-a-bitmap-to-grayscale-in-android
    public Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }
}



