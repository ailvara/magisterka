package eu.eglasses.robot.example;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.parrot.arsdk.arcommands.ARCOMMANDS_JUMPINGSUMO_ANIMATIONS_JUMP_TYPE_ENUM;
import com.parrot.arsdk.arcontroller.ARDeviceController;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sylwia Mikolajczuk
 * The activity for communication
 * Translates robot commands
 */
public class RobotHandler {

    ARDeviceController deviceController;
    String TAG = "RobotHandler";

    static final int MOVE = 1;
    static final int STOP_MOVE = 2;
    static final int TURN = 3;
    static final int STOP_TURN = 4;
    static final int JUMP_LONG = 5;
    static final int JUMP_FAR = 6;
    static final int PICTURE = 7;

    /**
     * Construct in context of device and the app
     * @param deviceController
     * @param context
     */
    RobotHandler(ARDeviceController deviceController, Context context)
    {
        this.deviceController = deviceController;
    }

    /**
     * Interpret the command from the server
     * @param args
     */
    public void execute(Object... args)
    {
        try {
            if (args == null) return;
            Log.i(TAG,""+args[0]);
            JSONObject command = (JSONObject) args[0];

            if (!command.has("type")) {
                Log.i(TAG, "No command!");
                return;
            }

            int type = command.getInt("type");
            int value;

            //Get value for some types of commands
            if (command.has("value")) {
                value = command.getInt("value");
                Log.i(TAG, "Type: " + type + ", value: " + value);
            }
            
            switch (type) {
                case MOVE:
                    if (!command.has("value")) break;
                    value = (int) command.get("value");
                    move(value);
                    break;
                case TURN:
                    if (!command.has("value")) break;
                    value = (int) command.get("value");
                    turn(value);
                    break;
                case STOP_MOVE:
                    stopMove();
                    break;
                case STOP_TURN:
                    stopTurn();
                    break;
                case JUMP_FAR:
                    jumpLong();
                    break;
                case JUMP_LONG:
                    jumpHigh();
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send frame from the robot to the server
     * @param content
     */
    public void sendImage(byte[] content)
    {
        String img = Base64.encodeToString(content, Base64.DEFAULT);
        Log.i("ConnectionTest", PICTURE + " " + img.length());
        InternetConnection.instance.new ExecuteConnection().execute(PICTURE, img); //ANDROID 6
    }

    /**
     * Rotate the robot
     * @param value the speed
     */
    public void turn(int value) //minus - left, plus - right
    {
        deviceController.getFeatureJumpingSumo().setPilotingPCMDTurn((byte) value);
        deviceController.getFeatureJumpingSumo().setPilotingPCMDFlag((byte) 1);
        //if(conn!=null) conn.new ExecuteConnection().execute(TURN, value);
    }

    /**
     * Stop robot from rotating
     */
    public void stopTurn()
    {
        deviceController.getFeatureJumpingSumo().setPilotingPCMDTurn((byte) 0);
        //if(conn!=null) conn.new ExecuteConnection().execute(STOP_TURN);
    }

    /**
     * Move the robot
     * @param value the speed
     */
    public void move(int value) //minus - backwards
    {
        deviceController.getFeatureJumpingSumo().setPilotingPCMDSpeed((byte) value);
        deviceController.getFeatureJumpingSumo().setPilotingPCMDFlag((byte) 1);
        //if(conn!=null) conn.new ExecuteConnection().execute(MOVE, value);
    }

    /**
     * Stop the robot from moving
     */
    public void stopMove()
    {
        deviceController.getFeatureJumpingSumo().setPilotingPCMDSpeed((byte) 0);
        //deviceController.getFeatureJumpingSumo().setPilotingPCMDFlag((byte) 0);
        //if(conn!=null) conn.new ExecuteConnection().execute(STOP_MOVE);
    }

    /**
     * Make a high jump
     */
    public void jumpHigh()
    {
        deviceController.getFeatureJumpingSumo().sendAnimationsJump(ARCOMMANDS_JUMPINGSUMO_ANIMATIONS_JUMP_TYPE_ENUM.ARCOMMANDS_JUMPINGSUMO_ANIMATIONS_JUMP_TYPE_HIGH);
        //if (conn != null) conn.new ExecuteConnection().execute(JUMP_HIGH);
    }

    /**
     * Make a long jump
     */
    public void jumpLong()
    {
        deviceController.getFeatureJumpingSumo().sendAnimationsJump(ARCOMMANDS_JUMPINGSUMO_ANIMATIONS_JUMP_TYPE_ENUM.ARCOMMANDS_JUMPINGSUMO_ANIMATIONS_JUMP_TYPE_LONG);
        //if(conn!=null) conn.new ExecuteConnection().execute(JUMP_FAR);
    }
}
