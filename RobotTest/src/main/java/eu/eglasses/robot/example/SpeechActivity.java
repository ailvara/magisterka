//Parts:
/** ====================================================================
 * Copyright (c) 2014 Alpha Cephei Inc.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALPHA CEPHEI INC. ``AS IS'' AND
 * ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CARNEGIE MELLON UNIVERSITY
 * NOR ITS EMPLOYEES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ====================================================================
 */

package eu.eglasses.robot.example;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;

/**
 * The activity for speech-recognition interaction
 */
public class SpeechActivity extends RobotActivity implements RecognitionListener
{
    private static final String MENU_SEARCH = "menu";
    private static final String TAG = "SpeechActivity";

    private RobotHandler robot;
    private SpeechRecognizer recognizer;

    /**
     * Init the activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(eu.eglasses.robot.example.R.layout.activity_plain);
        imgView = (ImageView) findViewById(R.id.preview);
        batteryLabel = (TextView) findViewById(R.id.batteryLabel);
        robot = new RobotHandler(deviceController, this);

        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO);
        if(permissionCheck == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, 1);
        }
        else runRecognizerSetup();

    }

    /**
     * Resume after getting the record audio permission
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                runRecognizerSetup();
                Log.i(TAG, "Request success");
            } else {
                Log.i(TAG, "Request fail");
                //finish();
            }
        }
    }

    /**
     * Run recognizer setup
     */
    private void runRecognizerSetup() {
        // Recognizer initialization is a time-consuming and it involves IO,
        // so we execute it in async task
        new AsyncTask<Void, Void, Exception>() {
            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    Log.i(TAG,"Run recognizer");
                    Assets assets = new Assets(SpeechActivity.this);
                    File assetDir = assets.syncAssets();
                    Log.i(TAG, "Got assets");
                    setupRecognizer(assetDir);
                } catch (IOException e) {
                    Log.i(TAG,e.getMessage());
                    return e;
                }
                Log.i(TAG, "Setup recognizer");
                return null;
            }

            @Override
            protected void onPostExecute(Exception result) {
                Log.i(TAG,"On post execute");
                if (result != null) {
                    Log.i(TAG,"Error: "+result.getMessage());
                } else {
                    Log.i(TAG,"Switch search");
                    switchSearch(MENU_SEARCH);
                }
            }
        }.execute();
    }

    /**
     * Shut down recognizer on destroy
     */
    @Override
    public void onDestroy() {
        super.onDestroy();

        if (recognizer != null) {
            recognizer.cancel();
            recognizer.shutdown();
        }
    }

    /**
     * Update during speech
     * @param hypothesis word found
     */
    @Override
    public void onPartialResult(Hypothesis hypothesis) {

    }

    /**
     * Process result after end of speech
     * Interpret commands and send to the robot
     * @param hypothesis
     */
    @Override
    public void onResult(Hypothesis hypothesis) {
        //Log.i(TAG,"On partial result");
        if (hypothesis == null)
            return;
        String text = hypothesis.getHypstr();
        //((TextView) findViewById(R.id.test)).setText(text);
        Log.i("Voice command:",text);

        //Polish version
        if(text.equals("naprzod")) robot.move(30);
        if(text.equals("lewo")) {
            robot.turn(-5);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            robot.stopTurn();
        }
        if(text.equals("prawo")) {
            robot.turn(5);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            robot.stopTurn();
        }
        if(text.equals("stop")) {
            robot.stopMove();
            robot.stopTurn();
        }
        if(text.equals("dotylu")) robot.move(-30);
        if(text.equals("daleko")) robot.jumpLong();
        if(text.equals("wysoko")) robot.jumpHigh();

        //English version
        if(text.equals("go")) robot.move(30);
        if(text.equals("left")) {
            robot.turn(-5);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            robot.stopTurn();
        }
        if(text.equals("right")) {
            robot.turn(5);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            robot.stopTurn();
        }
        if(text.equals("stop")) {
            robot.stopMove();
            robot.stopTurn();
        }
        if(text.equals("back")) robot.move(-30);
        if(text.equals("jump")) robot.jumpHigh();
    }

    /**
     * Spot beginning of speech
     */
    @Override
    public void onBeginningOfSpeech() {
        Log.i(TAG,"Speech start");
    }

    /**
     * After speech start listening again
     */
    @Override
    public void onEndOfSpeech() {
        switchSearch(MENU_SEARCH);
    }

    /**
     * Start listening
     * @param searchName
     */
    private void switchSearch(String searchName) {
        recognizer.stop();
        recognizer.startListening(searchName, 10000);
    }

    /**
     * Setup the speech recognizer, load models
     * @param assetsDir
     * @throws IOException
     */
    private void setupRecognizer(File assetsDir) throws IOException {
        // The recognizer can be configured to perform multiple searches
        // of different kind and switch between them

        File model = new File(assetsDir, "en-us-ptm");
        File dict = new File(assetsDir, "cmudict-en-us.dict");
        Log.i(TAG,"Setup recognizer");
        recognizer = SpeechRecognizerSetup.defaultSetup()
                .setAcousticModel(model)
                .setDictionary(dict)
                .setRawLogDir(assetsDir) // To disable logging of raw audio comment out this call (takes a lot of space on the device)
                //.setKeywordThreshold(1e-10f) // Threshold to tune for keyphrase to balance between false alarms and misses
                .setBoolean("-allphone_ci", true)  // Use context-independent phonetic search, context-dependent is too slow for mobile
                .getRecognizer();
        recognizer.addListener(this);

        // Create grammar-based search for selection between demos
        File menuGrammar = new File(assetsDir, "menu.gram");
        recognizer.addGrammarSearch(MENU_SEARCH, menuGrammar);

        Log.i(TAG, "Grammar done");
    }

    /**
     * On error
     * @param error
     */
    @Override
    public void onError(Exception error) {
    }

    /**
     * On timeout start again
     */
    @Override
    public void onTimeout() {
        switchSearch(MENU_SEARCH);
    }
}



